class Dog:
  #constructor
  def __init__(self, name, id, breed):
    self.name = name
    self.id = id
    self.breed = breed

#getters and setters
  def get_name(self):
    return self.name
  def set_name(self, name):
    self.name = name
  def get_id(self):
    return self.id
  def set_id(self, id):
    self.id = id
  def get_breed(self):
    return self.breed
  def set_breed(self, breed):
    self.breed = breed
  def print_dog(self):
    print("ID:" + str(self.id)+ ", Name: " + str(self.name) + ", Breed: " + str(self.breed))

