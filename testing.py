

import unittest
import Dog
import Adoption
import Shelter
import Customer





class TestAdoption(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("setUpClass()")
        cls.shelter = Shelter.Shelter().get()

        cls.johnny = Customer.Customer("Johnny")
        cls.isabelle = Customer.Customer("Isabelle")
        cls.frank = Customer.Customer("Frank")

        cls.dog_1 = Dog.Dog("Max",1,"Poodle")
        cls.dog_2 = Dog.Dog("Lulu",2,"Shitzu")
        cls.dog_3 = Dog.Dog("Samantha",3,"Lab")
        cls.dog_4 = Dog.Dog("Buddy",4,"Lab")
        cls.dog_5 = Dog.Dog("Gerald", 5, "Pug")

        cls.shelter.add_dog(cls.dog_1)
        cls.shelter.add_dog(cls.dog_2)
        cls.shelter.add_dog(cls.dog_3)
        cls.shelter.add_dog(cls.dog_4)

        cls.isabelle.adopt(cls.dog_5)

        cls.shelter.add_customer(cls.johnny)
        cls.shelter.add_customer(cls.isabelle)
        cls.shelter.add_customer(cls.frank)




    @classmethod
    def tearDownClass(cls):
        print('tearDownClass()')


    def test_add_adoption_correct(self):
        print('test add adoption correct')

        #adopt a dog that is in the shelter
        test = self.shelter.add_adoption_correct(self.dog_1.get_id(), self.johnny)
        self.assertTrue(test)
        
        #remove dog from the available dogs list
        self.assertNotIn(self.dog_1, self.shelter.get_dogs())

        #added an adoption to the adoptions list
        self.assertEqual(len(self.shelter.get_adoptions()), 1)

        #adopting a dog that isn't in the shelter
        test = self.shelter.add_adoption(-1, self.johnny)
        self.assertFalse(test)


    def test_add_adoption_incorrect(self):
        print("add adoption incorrect")
        # adopt a dog that is in the shelter
        test = self.shelter.add_adoption_incorrect(self.dog_1.get_id(), self.johnny)
        self.assertTrue(test)

        # remove dog from the available dogs list
        self.assertNotIn(self.dog_1, self.shelter.get_dogs())

        # added an adoption to the adoptions list
        self.assertEqual(len(self.shelter.get_adoptions()), 1)

        # adopting a dog that isn't in the shelter
        test = self.shelter.add_adoption(-1, self.johnny)
        self.assertFalse(test)

    def test_return_dog(self):
        print('test return dog')

        #return a dog :(
        test = self.shelter.return_dog(5, "Isabelle")
        self.assertTrue(test)
        
        #make sure dog has been removed from Isabelle's dogs :(
        self.assertNotIn(self.dog_5, self.isabelle.get_adoptions())

        #make sure dog has been added back to the shelter
        self.assertIn(self.dog_5, self.shelter.get_dogs())

        #test a customer that isn't real
        test = self.shelter.return_dog(5, "Nobody")
        self.assertFalse(test)

        #test a dog that isn't real
        test = self.shelter.return_dog(-1, "Isabelle")
        self.assertFalse(test)


    def test_add_dog(self):
        d = Dog.Dog("Cornelius", 6, "Blood Hound")
        self.shelter.add_dog(d)
        #check that new dog was added to list of dogs in shelter, not adoptions
        self.assertIn(d,self.shelter.dogs)
        self.assertNotIn(d,self.shelter.adoptions)


    def test_get_breed(self):

        print('test get by breed')
        rc = self.shelter.get_by_breed("Lab")
        #should return true, and print list of dogs with breed of Lab
        self.assertTrue(rc)
        #should return false
        rc = self.shelter.get_by_breed("Rottweiler")
        self.assertFalse(rc)

    




if __name__ == "__main__":
    unittest.main()




