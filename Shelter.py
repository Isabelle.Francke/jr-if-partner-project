from Adoption import Adoption
from Customer import Customer
from Dog import Dog

class Shelter:

  s_shelter = None

  def __init__(self):
    self.customers = set()
    self.dogs = set()
    self.adoptions = set()

  @classmethod
  def get(self):
    if self.s_shelter is None:
        self.s_shelter = Shelter()
    return self.s_shelter


  #getters and setters
  def get_customers(self):
    return self.customers
  def set_customers(self, customers):
    self.customers = customers
  def get_dogs(self):
    return self.dogs
  def set_dogs(self, dogs):
    self.dogs = dogs
  def get_adoptions(self):
    return self.adoptions
  def set_adoptions(self, adoptions):
    self.adoptions = adoptions

  #creates new dog and appends it to dog set
  def add_dog(self,dog):
    self.dogs.add(dog)

  #verifies if dog is available, removes it from dogs set and places it into adoptions set
  def add_adoption_correct(self, id,customer):

    for i in self.dogs:
      if(i.id == id):
        customer.adopt(i)
        self.adoptions.add(Adoption(i, customer))
        self.dogs.remove(i)
        print("Dog Adopted")
        return True

    print("Dog Unavailable")
    return False

  #incorrect implementation --> should remove from dogs AND add to adoptions
  def add_adoption_incorrect(self, id,customer):

    for i in self.dogs:
      if(i.id == id):
        customer.adopt(i)
        self.adoptions.add(Adoption(i, customer))
        print("Dog Adopted")
        return True

    print("Dog Unavailable")
    return False

  #adds the dog back to the list of available dogs, deletes associated adoption, uses Customer return_dog to remove dog from their dog set
  def return_dog(self, id, custName):

    for i in self.customers:
      if(i.name == custName):
        for j in i.adoptions:
          if(j.id == id):
            i.return_dog(id)
            self.dogs.add(j)
            print("Dog Returned")
            return True

    print("Invalid Return")
    return False



  #returns a list of dogs with the requested breed
  def get_by_breed(self, breed):
      exists = False
      for i in self.dogs:
          if(i.breed == breed):
            exists = True
            i.print_dog()
      if not exists:
        print("No dogs of that breed")
      return exists

  def add_customer(self,Customer):
      self.customers.add(Customer)