from Customer import Customer
from Dog import Dog

class Adoption:
  #constructor
  def __init__(self, customer, dog):
    self.customer = customer
    self.dog = dog

  #getters and setters
  def get_customer(self):
    return self.customer
  def set_customer(self, customer):
    self.customer = customer
  def get_dog(self):
    return self.dog
  def set_dog(self, dog):
    self.dog = dog