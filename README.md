# JR-IF Partner Project

This is the partner project for CS205 by Isabelle Francke and John Richardson. The program simulates a dog shelter using object-oriented design. A Shelter contains sets of Dogs, Customers, and Adoptions. An Adoption has exactly one Customer and exactly one Dog. A Customer also has a list of adopted dogs.   
The main function tests various methods, but there is also a complete suite of unit tests for several methods in Shelter in testing.py. One unit test, test_add_adoption_incorrect, is designed to fail, so running testing.py should result in one failed test. 