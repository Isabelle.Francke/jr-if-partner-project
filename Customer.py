
from Dog import Dog


class Customer:
  #constructor
  def __init__(self, name):
    self.name = name
    self.adoptions = set()

  #getters and setters
  def get_name(self):
    return self.name
  def set_name(self, name):
    self.name = name
  def get_adoptions(self):
    return self.adoptions
  def set_adoptions(self,adoptions):
    self.adoptions = adoptions

  #adds the selected dog to the dog set
  def adopt(self, dog):
      self.adoptions.add(dog)

  #returns list of all dogs of a particular breed, allows customer to select dog from list
  #def adopt(self, breed):

  #returns the dog to the shelter if id matches dog in the customer's set
  def return_dog(self, id):
      for i in self.adoptions:
        if(i.id == id):
          returned = i
          
          
      self.adoptions.remove(returned)
    
