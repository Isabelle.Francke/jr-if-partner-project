
import Shelter
import Adoption
import Dog
import Customer



def runtime_test(shelter):

    #initialize customers/dogs, add dogs and customers to shelter lists

    johnny = Customer.Customer('Johnny')
    isabelle = Customer.Customer('Isabelle')

    shelter.add_customer(johnny)
    shelter.add_customer(isabelle)

    dog_1 = Dog.Dog("Max", 1, "Poodle")
    dog_2 = Dog.Dog("Lulu", 2, "Shitzu")
    dog_3 = Dog.Dog("Samantha", 3, "Lab")
    dog_4 = Dog.Dog("Buddy", 4, "Lab")

    shelter.add_dog(dog_1)
    shelter.add_dog(dog_2)
    shelter.add_dog(dog_3)
    shelter.add_dog(dog_4)

    jadd = shelter.add_adoption_correct(2,johnny)

    #should be true, dog is available
    print(jadd)

    iadd = shelter.add_adoption_correct(2,isabelle)
    #should be false, dog unavailable
    print(iadd)

    #should print dog_3 and dog_4
    gbbT = shelter.get_by_breed("Lab")
    #true, there are labs
    print(gbbT)

    gbbF = shelter.get_by_breed("Rottweiler")
    #false, no rottweilers
    print(gbbF)

    #return dog and let isabelle now adopt
    rdJ = shelter.return_dog(2,"Johnny")
    #should be true
    print(rdJ)
    #now isabelle adopts
    adI = shelter.add_adoption_correct(2,isabelle)
    #should be true, dog is returned an available
    print(adI)



def main():
    shelter = Shelter.Shelter().get()
    runtime_test(shelter)

main()